public class MyDateTime{
  int day;
  int month;
  int year;
  int hour;
  int minute;
  int[] maxDays = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

  public MyDateTime(MyDate date,MyTime time){
    day = date.day;
    month = date.month;
    year = date.year;
    hour = time.hour;
    minute = time.minute;
  }

  public void incrementHour(int value){
    if(value < 0){
      decrementHour(-value);
    }

    else{
      hour += value;
      if (hour > 23){
        int extday = hour / 24;
        hour = hour % 24;
        incrementDay(extday);
      }
    }
  }

  public void decrementHour(int value){
    if(value < 0){
      incrementHour(-value);
    }
    else{
      hour -= value;
      if(hour < 0){
        int extday = (hour * -1) / 24 + 1;
        decrementDay(extday);
        hour = 24 - (hour * -1) % 24;
      }
    }
  }

  public void incrementHour(){
    incrementHour(1);
  }

  public void decrementHour(){
    decrementHour(1);
  }

  public void incrementMinute(int value){
    if(value < 0){
      decrementMinute(-value);
    }
    else{
      minute += value;
      if(minute > 59){
        int exthour = minute / 60;
        incrementHour(exthour);
        minute = minute % 60;
      }
    }
  }

  public void decrementMinute(int value){
    if(value < 0){
      incrementMinute(-value);
    }
    else{
      minute -= value;
      if(minute < 0){
        int exthour = (minute * -1) / 60 + 1;
        decrementHour(exthour);
        minute = 60 - (minute * -1) % 60;
      }
    }
  }

  public void incrementMinute(){
    incrementMinute(1);
  }

  public void decrementMinute(){
    decrementMinute(1);
  }


  public void incrementDay() {
		int newDay = day+1;
		if (newDay > maxDays[month]){
			day =1;
			incrementMonth();
		}else if(month == 1 && newDay ==29 && !inLeapYear()){
			day =1;
			incrementMonth();	//will also handle the change in year
		}else{
			day = newDay;
		}

	}


	public void decrementDay() {
		int newDay = day - 1;
		if (newDay == 0){
			day = 31;
			decrementMonth(); //will also adjust the day to its max and handle the change in year
		}else{
			day = newDay;
		}

	}

	public void incrementDay(int i) {
		while (i > 0){
			incrementDay();
			i--;
		}

	}

	public void decrementDay(int i) {
		while (i > 0){
			decrementDay();
			i--;
		}

	}


	public void incrementMonth(int i) {
		int newMonth = (month + i) % 12;
		int yearChange = 0;

		if (newMonth < 0) {
			newMonth += 12;
			yearChange = -1;
		}

		yearChange += (month + i) / 12;
		month = newMonth;
		year += yearChange;
		if (day > maxDays[month]){
			day = maxDays[month];
			if(month ==1 && day ==29 && !inLeapYear()){
				day =28;
			}
		}
	}


	public void incrementMonth() {
		incrementMonth(1);

	}

	public void decrementMonth(int i) {
		incrementMonth(-i);

	}

	public void decrementMonth() {
		incrementMonth(-1);

	}

	public void incrementYear(int i) {
		year+=i;
		if (month ==1 && day ==29 && !inLeapYear()){
			day = 28;
		}

	}

	public void incrementYear() {
		incrementYear(1);

	}

	public void decrementYear() {
		incrementYear(-1);
	}


	public void decrementYear(int i) {
		incrementYear(-i);

	}



	public boolean isBefore(MyDateTime anotherDateTime) {
    Long a = Long.parseLong(toString().replaceAll("[- :]" , "")) ;
		Long b = Long.parseLong(anotherDateTime.toString().replaceAll( "[- :]" , ""));
		return a < b;
	}

	public String dayTimeDifference(MyDateTime anotherDateTime) {
		int diff = 0;
		if (isBefore(anotherDateTime)){
			MyDateTime dateTime = new MyDateTime(new MyDate(day,month+1,year),new MyTime(hour,minute)); //a copy of current object
			while (dateTime.isBefore(anotherDateTime)){
				dateTime.incrementMinute();
				diff++;
			}
		}else if(isAfter(anotherDateTime)){
			MyDateTime dateTime = new MyDateTime(new MyDate(day,month+1,year),new MyTime(hour,minute)); //a copy of current object
			while (dateTime.isAfter(anotherDateTime)){
				dateTime.decrementMinute();
				diff++;
			}

		}
    String result = "";
    int minutes[] = {518400,43200,1440,60,1};
    String timevar[] = {"year(s)","month(s)","day(s)","hour(s)","minute(s)"};
    for(int i = 0 ; i < 5 ; i++){
      int divide = diff / minutes[i];
      if(divide > 0){
        result += divide + timevar[i] + " ";
      }
      diff = diff % minutes[i];
    }
		return result;
	}

	public boolean isAfter(MyDateTime anotherDateTime) {
		Long a = Long.parseLong(toString().replaceAll("[: -]", ""));
		Long b = Long.parseLong(anotherDateTime.toString().replaceAll("[: -]", ""));
		return a > b;
	}

  public String toString(){
    return year + "-"+(month + 1 < 10 ? "0" : "") + (month + 1) + "-" +(day < 10 ? "0" : "") + day + " " + (hour < 10 ? "0" : "") + hour + ":" + (minute < 10 ? "0" : "") + minute;
  }

	public boolean inLeapYear(){
		return year % 4 == 0 ? true : false;
	}
}
