
public class MyDate {
	int day;
	int month; // should be incremented by one before printing
	int year;

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month-1; //0 represents January  11 represents December
		this.year = year;
	}
}
