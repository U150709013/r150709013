package shapes.model;

public class Point {
	
	private int x;
	private int y;
	
	
	public Point(int x, int y){
		
		this.x = x;
		this.y = y;

	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public double distanceFromPoint(Point point){
		return Math.sqrt( Math.pow( ( this.x - point.x ) , 2 ) + Math.pow( (this.y - point.y) , 2 ) );
	}
	
	public String toString() {
		return "(x = " + getX() +", " +  "y = " + getY() + ")" ;
	}
}
