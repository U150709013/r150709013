package shapes.model;
import shapes.model.Point;

public class Line{
	
	private Point p1;
	private Point p2;
	
	public Line(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public double length(){
		return Math.sqrt( Math.pow( ( p1.getX() - p2.getX() ) , 2 ) + Math.pow( (p1.getY() - p2.getY() ) , 2 ) );
	}

	public Point getP1() {
		return p1;
	}

	public Point getP2() {
		return p2;
	}
	
	public String toString() {
		return this.p1.toString() + ", " + this.p2.toString();
	}
}
