package shapes.model;

import java.util.ArrayList;

public class PolyLine{
	
	private ArrayList<Point> points = new ArrayList<Point>() ;
	
	
	public PolyLine (){	
	}
	
	public PolyLine (PolyLine pl){
		addPolyLine(pl);
	}
	
	public PolyLine (Line ln){
		addLine(ln);
	}
	
	public PolyLine (Point pt){
		addPoint(pt);
	}
	
	public void addPoint (Point p){
		points.add(p);
	}
	
	public void addLine (Line l){
		points.add(l.getP1());
		points.add(l.getP2());
		
	}
	
	public double length(){
		double totallength = 0;
		for(int i = 0; i < points.size()-1 ; i++){
			totallength += points.get(i).distanceFromPoint(points.get(i+1));
		}
		return totallength;
	}
	
	public void addPolyLine(PolyLine pl){
		for (int i = 0 ; i < pl.points.size() ; i++){
			points.add(pl.points.get(i));
		}
	}
	
	public PolyLine reverse(){
		PolyLine rvrs = new PolyLine();
		
		for(int i = this.points.size() - 1 ; i >= 0 ; i--){
			rvrs.points.add(this.points.get(i));
		}
		return rvrs;
	}
	
	
	public String toString() {
		String printer = "[";
		
		for (int i = 0 ; i < this.points.size()-1 ; i++){
			printer += this.points.get(i) + ", ";
		}
		printer += this.points.get(this.points.size()-1) + "]";
		return printer;
	}
}
