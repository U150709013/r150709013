public class MyDate{
  int day;
  int month;
  int year;

  public MyDate(int d,int m,int y){
    day = d;
    month = m;
    year = y;
  }

  public String toString(){
    return year + "-" + month + "-" + day;
  }

  public String incrementDay(){
    if ( ((month == 4) || (month == 6) || (month == 9) || (month == 11)) && (day == 30) ){
      month++;
      day = 1;
    }

    else if( ((month == 1) || (month == 3) || (month == 7) || (month == 8) || (month == 10) || (month == 12) || (month == 5)) && (day == 31) ){
      month++;
      day = 1;

    }else if( month == 2 && day == 28 && year % 4 == 0){
      day++;
    }else if( month == 2 && day == 29){
      month++;
      day = 1;
    }else if( month == 2 && day == 28){
      month++;
      day = 1;
    }else{
      day++;
    }

    return year + "-" + month + "-" + day;
  }

  public String incrementYear(int value){
    year = year + value;
    return year + "-" + month + "-" + day;
  }

  public String decrementDay(){
    if( day == 1 && ( (month == 5) || (month == 7) || (month == 10) || (month == 12) ) ){
      month--;
      day = 30;
    }
    else if( day == 1 && ( (month == 2) || (month == 4) || (month == 8) || (month == 9) || (month == 11) || (month == 10) ) ){
      month--;
      day = 31;
    }else if(day == 1 && (month == 1) ){
      year--;
      month = 12;
      day = 31;
    }else if( day == 1 && month == 3 && year % 4 == 0) {
      month--;
      day = 29;
    }else if( day == 1 && month == 3) {
      month--;
      day = 28;
    }else{
      day--;
    }

    return year + "-" + month + "-" + day;
  }

  public String decrementYear(){
    if(month == 2 && day == 29){
      day = 28;
    }
    year--;
    return year + "-" + month + "-" + day;
  }
  public String decrementMonth(){
    if(month == 1){
      month = 12;
      year--;
    }else{
      month--;
    }

    return year + "-" + month + "-" + day;
  }

}
