package hw6;

public class Main {

	public static void main(String[] args) {

		Shape cr1 = new Circle();
		System.out.println(cr1);
		System.out.println(cr1.getArea());
		System.out.println(cr1.getPerimeter());
		
		System.out.println();
		
		Shape cr2 = new Circle(5);
		System.out.println(cr2);
		System.out.println(cr2.getArea());
		System.out.println(cr2.getPerimeter());

		System.out.println();
		
		Shape cr3 = new Circle(7.5 ,"cyan",true);
		System.out.println(cr3);
		System.out.println(cr3.getArea());
		System.out.println(cr3.getPerimeter());
		
		System.out.println("--------------------------------------------------------------------------------------------------------------");

		Shape rc1 = new Rectangle();
		System.out.println(rc1);
		System.out.println(rc1.getArea());
		System.out.println(rc1.getPerimeter());
		
		System.out.println();
		
		Shape rc2 = new Rectangle(5,9);
		System.out.println(rc2);
		System.out.println(rc2.getArea());
		System.out.println(rc2.getPerimeter());

		System.out.println();
		
		Shape rc3 = new Rectangle(14,5,"aqua",false);
		System.out.println(rc3);
		System.out.println(rc3.getArea());
		System.out.println(rc3.getPerimeter());
		
		System.out.println("--------------------------------------------------------------------------------------------------------------");

		Shape sq1 = new Square();
		System.out.println(sq1);
		System.out.println(sq1.getArea());
		System.out.println(sq1.getPerimeter());
		
		System.out.println();
		
		Shape sq2 = new Square(9);
		System.out.println(sq2);
		System.out.println(sq2.getArea());
		System.out.println(sq2.getPerimeter());

		System.out.println();
		
		Shape sq3 = new Square(3,"cyan",true);
		System.out.println(sq3);
		System.out.println(sq3.getArea());
		System.out.println(sq3.getPerimeter());

	}

}
