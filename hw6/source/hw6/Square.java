package hw6;

public class Square extends Rectangle {
	
	public Square(){
		
		super(1,1);
		
	}
	
	public Square(double side){
		
		super(side,side);
		
	}
	
	public Square(double side, String color, Boolean filled){
		
		super(side,side,color,filled);
		
	}
	
	public double getSide(){
		
		return super.getWidth();
		
	}
	
	public void setSide(double side){
		
		super.setLength(side);
	}
	
	@Override
	public void setLength(double length){
		
		super.setLength(length);
		super.setWidth(length);
	
	}
	
	@Override
	public void setWidth(double length){
		
		super.setWidth(length);
		super.setLength(length);
	
	}
	
	@Override
	public String toString(){
		return "A Square with side=" + super.getLength() + ", which is a subclass of " + super.toString();
	}
	
	
	
}
