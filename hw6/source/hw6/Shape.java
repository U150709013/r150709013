package hw6;

public abstract class Shape {
	
	private String color;
	private Boolean filled;
	
	public Shape(){
		
		this.color = "red";
		this.filled = true;
		
	}
	
	public Shape(String color, Boolean filled){
		
		this.color = color;
		this.filled = filled;
		
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Boolean isFilled() {
		return filled;
	}

	public void setFilled(Boolean filled) {
		this.filled = filled;
	}
	
	public String toString(){
		
		return "A Shape with color of " + color + " and " + (filled ? "filled" : "not filled"); 
		
	}
	
	public abstract double getArea();
	public abstract double getPerimeter();
	

}
