package hw6;

public class Circle extends Shape {
	
	private double radius;
	
	public Circle(){
		
		this.setRadius(1.0);
		
	}
	
	public Circle(double radius){
		
		this.setRadius(radius);
		
	}
	
	public Circle(double radius, String color, Boolean filled){
		
		this.setRadius(radius);
		this.setColor(color);
		this.setFilled(filled);
		
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}

	@Override
	public double getPerimeter() {
		return Math.PI * radius * 2;
	}
	
	@Override
	public String toString(){
		return "A Circle with radius=" + radius + ",which is a subclass of " + super.toString();
	}

}
