package hw6;

public class Rectangle extends Shape {
	
	private double length;
	private double width;
	
	public Rectangle(){
		
		this.width = 1.0;
		this.length = 1.0;
	
	}
	
	public Rectangle(double width,double length){
		
		this.length = length;
		this.width = width;
		
	}
	
	public Rectangle(double width, double length, String color, Boolean filled){
		
		this.length = length;
		this.width = width;
		this.setColor(color);
		this.setFilled(filled);
		
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public double getArea() {
		return width * length;
	}

	@Override
	public double getPerimeter() {
		return 2 * ( width + length );
	}
	
	@Override
	public String toString(){
		return "A Rectangle with width=" + width + " and length=" + length + ", which is a subclass of " + super.toString() ;
	}

}
