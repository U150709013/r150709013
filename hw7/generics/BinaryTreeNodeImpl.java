package generics;

public class BinaryTreeNodeImpl<T> implements BinaryTreeNode<T> {
	
	private T value;
	private BinaryTreeNode left;
	private BinaryTreeNode right;
	
	public BinaryTreeNodeImpl(T value){
		this.value = value;
	}
	
	public BinaryTreeNodeImpl(){
		
	}

	public void setValue(T value) {
		// TODO Auto-generated method stub
		this.value = value;
		
		
	}

	public void setLeft(BinaryTreeNode left) {
		// TODO Auto-generated method stub
		this.left = left;
		
	}

	public void setRight(BinaryTreeNode right) {
		// TODO Auto-generated method stub
		this.right = right;
		
	}

	public T getValue() {
		// TODO Auto-generated method stub
		return this.value;
	}

	public BinaryTreeNode getRight() {
		// TODO Auto-generated method stub
		return this.right;
	}

	public BinaryTreeNode getLeft() {
		// TODO Auto-generated method stub
		return this.left;
	}

}
