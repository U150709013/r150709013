package generics;

public interface BinaryTreeNode<T> {
	
	public void setValue(T value);
	public void setLeft(BinaryTreeNode<T> left);
	public void setRight(BinaryTreeNode<T> right);
	public T getValue();
	public BinaryTreeNode<T> getRight();
	public BinaryTreeNode<T> getLeft();

}
