package collections;

import java.util.*;

public class Course {
	
	List<Student> courselist = new ArrayList<Student>();
	
	String cname;
	String cinfo;

	public Course(String name, String info) {

		this.cname = name;
		this.cinfo = info;
		
	}
	
	public void addStudent(Student student){
		
		courselist.add(student);
		
	}
	
	public String toString(){
		
		return "Course = " + this.cname + " " + this.cinfo;
		
	}

	public Student getStudent(int i) {
		for (Student stu: courselist){
			if(stu.sid == i){
				return stu;
			}
		}
		return null;
	}

	public void setGrade(Student student, int i) {

		student.sgrade = i;
		
	}

	public List<Student> listStudentsOrderById() {
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s1.sid - s2.sid; // use your logic
		    }
		};
		Collections.sort(courselist, comparator);
		
		return courselist;

	}

	public List<Student> listStudentsOrderByName() {
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s1.sname.compareTo(s2.sname);
		    }
		};
		Collections.sort(courselist, comparator);
		
		return courselist;
	}

	public List<Student> listStudentsOrderBySurname() {
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s1.ssurname.compareTo(s2.ssurname);
		    }
		};
		Collections.sort(courselist, comparator);
		
		return courselist;
	}

	public Map<Student, Integer> listStudentGradesOrderbyId() {
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s1.sid - s2.sid; // use your logic
		    }
		};
		Collections.sort(courselist, comparator);
		
		Map<Student, Integer> map = new LinkedHashMap<Student, Integer>();

		for(Student st : courselist){
			map.put(st, st.sgrade);
		}
		
		return map;
	}

	public Map<Student, Integer> listStudentGradesOrderbyName() {
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s1.sname.compareTo(s2.sname);
		    }
		};
		Collections.sort(courselist, comparator);
		
		Map<Student, Integer> map = new LinkedHashMap<Student, Integer>();

		for(Student st : courselist){
			map.put(st, st.sgrade);
		}
		
		return map;
	}

	public Map<Student, Integer> listStudentGradesOrderbyGrade() {
		
		listStudentGradesOrderbyId();
		
		Comparator<Student> comparator = new Comparator<Student>() {
		    @Override
		    public int compare(Student s1, Student s2) {
		        return s2.sgrade - s1.sgrade;
		    }
		};
		Collections.sort(courselist, comparator);
		
		Map<Student, Integer> map = new LinkedHashMap<Student, Integer>();

		for(Student st : courselist){
			map.put(st, st.sgrade);
		}
		
		return map;
	}
	

}
