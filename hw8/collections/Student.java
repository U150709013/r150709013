package collections;

public class Student {

	String sname, ssurname;
	int sid, sgrade;
	
	public Student(String name, String surname, int id) {
		
		this.sname = name;
		this.ssurname = surname;
		this.sid = id;
		
	}
	
	public String toString(){
		return "id=" + this.sid + ", name=" + this.sname + ", surname=" + this.ssurname;
	}

}
